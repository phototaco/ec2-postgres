#!/bin/bash

# Install necessary packages
yum install wget -y

# Download and extract Node Exporter
wget https://github.com/prometheus/node_exporter/releases/download/v1.8.1/node_exporter-1.8.1.linux-amd64.tar.gz
tar zxvf node_exporter-1.8.1.linux-amd64.tar.gz
nohup node_exporter-1.8.1.linux-amd64/node_exporter > node_exporter.log &

# Install PostgreSQL
yum install postgresql15.x86_64 -y
yum install postgresql15-server.x86_64 -y

# Create a folder on SSD for the DB
# mkdir /var/lib/pgsql/data
# chown postgres:postgres /var/lib/pgsql/data
# chmod 750 /var/lib/pgsql/data

# Initialize PostgreSQL
sudo su - postgres << EOF
export PGDATA=/var/lib/pgsql/data

pg_ctl init

# Update PostgreSQL configuration
mv /var/lib/pgsql/data/postgresql.conf /var/lib/pgsql/data/postgresql.conf.orig
cat << EOCONF > /var/lib/pgsql/data/postgresql.conf
# General settings
listen_addresses = '*' # what IP address(es) to listen on;
max_connections = 350 # Lowering this to 200 to save memory

# Memory settings
shared_buffers = 3GB # min 128kB
work_mem = 128MB # min 64kB
maintenance_work_mem = 1GB # min 1MB
max_parallel_workers_per_gather = 2
max_parallel_workers = 4
effective_cache_size = 6GB   # Adjusted to fit within the overall 2GB limit

# WAL settings
wal_level = minimal # minimal, replica, or logical
fsync = off
max_wal_senders = 0                     # required for minimal wal_level
wal_buffers = 16MB # Fixed size buffer for WAL
checkpoint_timeout = 15min # range 30s-1d
checkpoint_completion_target = 0.9 # checkpoint target duration, 0.0 - 1.0
max_wal_size = 3GB
min_wal_size =  2GB
wal_writer_delay = 10ms # 1-10000 milliseconds
wal_compression = on


# Autovacuum settings
autovacuum = on # Enable autovacuum subprocess?  'on'
autovacuum_max_workers = 4 # max number of autovacuum subprocesses
autovacuum_naptime = 1min # time between autovacuum runs
autovacuum_vacuum_threshold = 50 # min number of row updates before vacuum
autovacuum_vacuum_insert_threshold = 50 # min number of row inserts before vacuum; -1 disables insert vacuums
autovacuum_analyze_threshold = 50 # min number of row updates before analyze
autovacuum_vacuum_scale_factor = 0.05 # fraction of table size before vacuum
autovacuum_vacuum_insert_scale_factor = 0.02 # fraction of inserts over table size before insert vacuum
autovacuum_analyze_scale_factor = 0.02 # fraction of table size before analyze
autovacuum_vacuum_cost_delay = 20ms # default vacuum cost delay for autovacuum, in milliseconds
autovacuum_vacuum_cost_limit = -1 # default vacuum cost limit for autovacuum, -1 means use vacuum_cost_limit

# Logging
logging_collector = on # Enable capturing of stderr, jsonlog, and csvlog into log files. Required to be on for csvlogs and jsonlogs.
log_directory = 'log' # directory where log files are written, can be absolute or relative to PGDATA
log_filename = 'postgresql.log'
log_min_duration_statement = 30 # -1 is disabled, 0 logs all statements and their durations, > 0 logs only statements running at least this number of milliseconds
log_autovacuum_min_duration = 0 # log autovacuum activity; -1 disables, 0 logs all actions and their durations, > 0 logs only actions running at least this number of milliseconds.
log_checkpoints = on
log_connections = on
log_disconnections = on
log_error_verbosity = default # terse, default, or verbose messages
log_line_prefix = '%t [%p]: db=%d,user=%u,app=%a,client=%h ' # special values:
log_lock_waits = on # log lock waits >= deadlock_timeout
log_temp_files = 0 # log temporary files equal or larger
log_timezone = 'America/Denver'

# Background Writer Settings
bgwriter_delay = 10ms  # 10ms between rounds
bgwriter_lru_maxpages = 1000  # max buffers written/round, 0 disables
bgwriter_lru_multiplier = 2.0  # 0-10.0 multiplier on buffers scanned/round

# synchronous_commit = off

max_files_per_process = 4096

# SSD settings
# effective_io_concurrency = 200 # recommended for SSDs

# Disable WAL archiving
archive_mode = off
archive_command = ''
EOCONF

mv /var/lib/pgsql/data/pg_hba.conf /var/lib/pgsql/data/pg_hba.conf.orig
cat << EOHBA > /var/lib/pgsql/data/pg_hba.conf
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     trust
# IPv4 local connections:
host    all             all             127.0.0.1/32            trust
host	all		all		10.0.2.1/24		trust
# IPv6 local connections:
host    all             all             ::1/128                 trust
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     trust
host    replication     all             127.0.0.1/32            trust
host    replication     all             ::1/128                 trust
EOHBA

# Start PostgreSQL
pg_ctl start -D /var/lib/pgsql/data

# Create role
psql -c "CREATE ROLE vulnpay LOGIN SUPERUSER PASSWORD 'vulnpay1';"
EOF

# Check if PostgreSQL started successfully
if pg_isready -q -d postgres; then
  echo "PostgreSQL is running"
else
  echo "Failed to start PostgreSQL"
  exit 1
fi

cd /var/lib/pgsql
get clone https://github.com/darold/pgbadger.git
chown postgres:postgres pgbadger
